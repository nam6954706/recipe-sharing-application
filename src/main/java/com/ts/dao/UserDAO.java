package com.ts.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ts.model.User;

@Service
public class UserDAO {
	@Autowired
	UserRepo userRepo;
	
	public UserDAO(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

	public User registerUser(User user) {
		// TODO Auto-generated method stub
		return  userRepo.save(user);
	}

	public List<User> getAllUsers() {
		// TODO Auto-generated method stub
		return userRepo.findAll();
	}
	
	

}
